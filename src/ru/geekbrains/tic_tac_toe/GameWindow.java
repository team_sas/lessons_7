package ru.geekbrains.tic_tac_toe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameWindow extends JFrame implements ActionListener {

    private Map map;
    private final JButton btnExit = new JButton("Выход");
    private final JButton btnNewGame = new JButton("Новая игра");

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GameWindow();
            }
        });
    }

    private GameWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setSize(500, 500);
        setTitle("Крестики-нолики");

        btnNewGame.addActionListener(this);
        btnExit.addActionListener(this);

        JPanel bottomPanel = new JPanel(new GridLayout(1, 2));
        bottomPanel.add(btnNewGame);
        bottomPanel.add(btnExit);
        add(bottomPanel, BorderLayout.SOUTH);

        map = new Map(3, 3);
        add(map, BorderLayout.CENTER);

        setResizable(false);
        setVisible(true);

        map.startNewGame();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if (src == btnNewGame) {
            map.startNewGame();
        } else if (src == btnExit) {
            System.exit(0);
        }
    }
}
