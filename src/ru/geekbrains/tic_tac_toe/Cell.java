package ru.geekbrains.tic_tac_toe;

import javax.swing.*;

public class Cell extends JPanel {

    private int x;
    private int y;
    private int startX;
    private int startY;
    private int endX;
    private int endY;

    Cell(int x, int y, int cellWidth, int cellHeight) {
        this.x = x + 1;
        this.y = y + 1;
        this.startX = x * cellWidth;
        this.startY = y * cellHeight;
        this.endX = x * cellWidth + cellWidth;
        this.endY = y * cellHeight + cellHeight;
    }

    public int getX() { return x; }

    public int getY() { return y; }

    public boolean isPointInside(int x, int y) {
        return x >= startX && x <= endX && y >= startY && y <= endY;
    }
}
