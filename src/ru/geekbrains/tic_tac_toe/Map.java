package ru.geekbrains.tic_tac_toe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Map extends JPanel {

    private int sizeX;
    private int sizeY;
    private Cell[][] cells;

    Map(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        cells = new Cell[sizeY][sizeX];
        setBackground(new Color(91, 190, 172));
        initListeners();
    }

    private void initMapCells() {
        final int w = getWidth() / sizeX; // cell width
        final int h = getHeight() / sizeY; // cell height
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                cells[y][x] = new Cell(x, y, w, h);
            }
        }
    }

    private void initListeners() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                mouseRelease(e);
            }
        });
    }

    private Cell getTargetCell(int x, int y) {
        for (int i = 0; i < sizeY; i++) {
            for (int j = 0; j < sizeX; j++) {
                if (cells[i][j].isPointInside(x, y)) return cells[i][j];
            }
        }
        return null;
    }

    private void mouseRelease(MouseEvent e) {
        Cell cell = getTargetCell(e.getX(), e.getY());
        if (cell != null) {
            System.out.println("Target cell x = " + cell.getX() + ", y = " + cell.getY());
        }
    }

    public void startNewGame() {
        initMapCells();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Color lineColor = new Color(76, 162, 146);
        g.setColor(lineColor);

        for (int i = 1; i < sizeX; i++) {
            int x = (getWidth() / sizeX) * i;
            g.drawLine(x, 0, x, getHeight());
        }

        for (int i = 1; i < sizeY; i++) {
            int y = (getHeight() / sizeY) * i;
            g.drawLine(0, y, getWidth(), y);
        }
    }
}
